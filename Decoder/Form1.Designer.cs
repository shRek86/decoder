﻿namespace Decoder
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_selectFile = new System.Windows.Forms.Button();
            this.tb_filePath = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btn_go = new System.Windows.Forms.Button();
            this.panelKoncentrat = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.l_progress = new System.Windows.Forms.Label();
            this.btn_findPoint = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_selectFile
            // 
            this.btn_selectFile.Location = new System.Drawing.Point(13, 13);
            this.btn_selectFile.Name = "btn_selectFile";
            this.btn_selectFile.Size = new System.Drawing.Size(75, 23);
            this.btn_selectFile.TabIndex = 0;
            this.btn_selectFile.Text = "Обзор";
            this.btn_selectFile.UseVisualStyleBackColor = true;
            this.btn_selectFile.Click += new System.EventHandler(this.btn_selectFile_Click);
            // 
            // tb_filePath
            // 
            this.tb_filePath.Location = new System.Drawing.Point(106, 13);
            this.tb_filePath.Name = "tb_filePath";
            this.tb_filePath.ReadOnly = true;
            this.tb_filePath.Size = new System.Drawing.Size(166, 20);
            this.tb_filePath.TabIndex = 1;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            this.openFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog_FileOk);
            // 
            // btn_go
            // 
            this.btn_go.Enabled = false;
            this.btn_go.Location = new System.Drawing.Point(13, 169);
            this.btn_go.Name = "btn_go";
            this.btn_go.Size = new System.Drawing.Size(259, 23);
            this.btn_go.TabIndex = 2;
            this.btn_go.Text = "Определить";
            this.btn_go.UseVisualStyleBackColor = true;
            this.btn_go.Click += new System.EventHandler(this.btn_go_Click);
            // 
            // panelKoncentrat
            // 
            this.panelKoncentrat.Location = new System.Drawing.Point(16, 94);
            this.panelKoncentrat.Name = "panelKoncentrat";
            this.panelKoncentrat.Size = new System.Drawing.Size(255, 69);
            this.panelKoncentrat.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(198, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Концентрация пятен (слева направо):";
            // 
            // l_progress
            // 
            this.l_progress.AutoSize = true;
            this.l_progress.Location = new System.Drawing.Point(16, 325);
            this.l_progress.Name = "l_progress";
            this.l_progress.Size = new System.Drawing.Size(0, 13);
            this.l_progress.TabIndex = 16;
            // 
            // btn_findPoint
            // 
            this.btn_findPoint.Enabled = false;
            this.btn_findPoint.Location = new System.Drawing.Point(106, 39);
            this.btn_findPoint.Name = "btn_findPoint";
            this.btn_findPoint.Size = new System.Drawing.Size(166, 23);
            this.btn_findPoint.TabIndex = 17;
            this.btn_findPoint.Text = "Распознавание пятен";
            this.btn_findPoint.UseVisualStyleBackColor = true;
            this.btn_findPoint.Click += new System.EventHandler(this.btn_findPoint_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(283, 198);
            this.Controls.Add(this.btn_findPoint);
            this.Controls.Add(this.l_progress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panelKoncentrat);
            this.Controls.Add(this.btn_go);
            this.Controls.Add(this.tb_filePath);
            this.Controls.Add(this.btn_selectFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Decoder v2.0";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_selectFile;
        private System.Windows.Forms.TextBox tb_filePath;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button btn_go;
        private System.Windows.Forms.Panel panelKoncentrat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label l_progress;
        private System.Windows.Forms.Button btn_findPoint;
    }
}

