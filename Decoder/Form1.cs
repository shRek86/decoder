﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Decoder
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string pathFile;
        private int[] rgbIntenc, borderIndex;
        private double[,] greyArray, lnArray;
        private double[] lnArea = new double[0];

        private Bitmap img;
        public Bitmap Img
        {
            set
            {
                img = value;
            }
        }

        private double[] lnGraph, inverseGraph;
        public double[] LnGraph
        {
            set
            {
                lnGraph = value;
            }
        }

        public double[] InverseGraph
        {
            set
            {
                inverseGraph = value;
            }
        }

        public int[] BorderIndex
        {
            set
            {
                borderIndex = value;
            }
        }


        private Point[] borderPoint;
        public Point[] BorderPoint
        {
            set
            {
                borderPoint = value;
            }
        }

        private void btn_selectFile_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
        }

        private void openFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            pathFile = openFileDialog.FileName;
            tb_filePath.Text = pathFile;
            SelectWorkArea swa = new SelectWorkArea(pathFile);
            swa.Show();
            swa.Focus();
        }

        
        private void btn_go_Click(object sender, EventArgs e)
        {
            int k = 0;

            int width = img.Width,
                height = img.Height;
            
            double sum = 0;
            int oldIndex = 0;
         /*   for (int j = 0; j < borderIndex.Length+1; j++)
            {
                int newIndex, countRow;

                if (j == borderIndex.Length)
                {
                    newIndex = height;
                }
                else
                {
                    newIndex = borderIndex[j];
                }
                countRow = 0;

                for (int i = oldIndex; i < newIndex; i++)
                {
                    //sum += lnGraph[i];
                    sum += inverseGraph[i];
                    countRow++;
                }
                k++;
                Array.Resize<double>(ref lnArea, k);
                //lnArea[k - 1] = sum / (countRow * width);
                lnArea[k - 1] = sum;
               // lnArea[k - 1] = sum/countRow;
                sum = 0;
                if (j != borderIndex.Length)
                {
                    oldIndex = borderIndex[j];
                }   
            }*/
            for (int j = 0; j < borderPoint.Length; j++)
            {

                for (int i = borderPoint[j].X; i < borderPoint[j].Y; i++)
                {
                    //sum += lnGraph[i];
                    sum += inverseGraph[i];
                }
                k++;
                Array.Resize<double>(ref lnArea, k);
                //lnArea[k - 1] = sum / (countRow * width);
                lnArea[k - 1] = sum;
                // lnArea[k - 1] = sum/countRow;
                sum = 0;
                if (j != borderIndex.Length)
                {
                    oldIndex = borderIndex[j];
                }
            }

            try
            {
                double[] konc = readKoncentrat();
                Plot pl = new Plot(inverseGraph, lnArea, konc);
                pl.Show();
            }
            catch (Exception ex)
            {
            }          
        }

        /// <summary>
        /// Считывание концентраций
        /// </summary>
        /// <returns></returns>
        private double[] readKoncentrat()
        {
            int count = borderIndex.Length + 1;
            double[] kon = new double[count];
            try
            {
           
                for(int i = 0; i < count; i++)
                {
                    TextBox tb = panelKoncentrat.Controls["tb_konc_" + (i+1).ToString()] as TextBox;
                    kon[i] = tb.Text.Length > 0 ? double.Parse(tb.Text.Replace('.',',')) : 0;
                }
                
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка. Возможны только цифры");
                throw new ArgumentException();
            }
            return kon;
        }


        /// <summary>
        /// Создаются поля для ввода концнгтрации
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private TextBox createTB(int id)
        {
            int wid = panelKoncentrat.Width / 6;
            TextBox tb = new TextBox();

            int offset = 0,
                x, y = 0;

            if(id != 1 && id != 6)
            {
                offset = 10;
            }

            x = wid * (id - 1) + offset * (id - 1);

            if (id > 5)
            {
                x = wid * (id - 5 - 1) + offset * (id - 5 - 1);
                y = tb.Height + 10;
            }
            
            tb.Name = "tb_konc_" + id.ToString();
            tb.Width = wid;
            tb.LostFocus += tb_LostFocus;
            tb.Location = new Point(x,y);
            return tb;
        }

        void tb_LostFocus(object sender, EventArgs e)
        {
            int cou = 0;
            for (int i = 0; i < panelKoncentrat.Controls.Count; i++)
            {
                TextBox tb = panelKoncentrat.Controls["tb_konc_" + (i + 1).ToString()] as TextBox;
                cou += (tb.Text.Length > 0) && (!tb.Text.Equals("0")) ? 1 : 0;
            }
            btn_go.Enabled = cou > 1 ? true : false;
        }

        public void startCreateTB(int _count)
        {
            panelKoncentrat.Controls.Clear();
            for (int i = 1; i <= _count; i++)
            {
                panelKoncentrat.Controls.Add(createTB(i));
            }
        }
        
        private void btn_findPoint_Click(object sender, EventArgs e)
        {
            new SplitImg(img).Show();
        }


    }
}
