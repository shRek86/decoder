﻿namespace Decoder
{
    partial class Plot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pv1 = new OxyPlot.WindowsForms.PlotView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.l_fut = new System.Windows.Forms.Label();
            this.l_natur = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pv1
            // 
            this.pv1.Location = new System.Drawing.Point(153, 12);
            this.pv1.Name = "pv1";
            this.pv1.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.pv1.Size = new System.Drawing.Size(738, 440);
            this.pv1.TabIndex = 1;
            this.pv1.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.pv1.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.pv1.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.l_fut);
            this.panel1.Controls.Add(this.l_natur);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(135, 317);
            this.panel1.TabIndex = 2;
            // 
            // l_fut
            // 
            this.l_fut.AutoSize = true;
            this.l_fut.Location = new System.Drawing.Point(6, 156);
            this.l_fut.Name = "l_fut";
            this.l_fut.Size = new System.Drawing.Size(0, 13);
            this.l_fut.TabIndex = 3;
            // 
            // l_natur
            // 
            this.l_natur.AutoSize = true;
            this.l_natur.Location = new System.Drawing.Point(6, 17);
            this.l_natur.Name = "l_natur";
            this.l_natur.Size = new System.Drawing.Size(0, 13);
            this.l_natur.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Аппроксимация:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Исходные данные:";
            // 
            // Plot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 464);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pv1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Plot";
            this.Text = "Расчёт";
            this.Load += new System.EventHandler(this.Plot_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private OxyPlot.WindowsForms.PlotView pv1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label l_fut;
        private System.Windows.Forms.Label l_natur;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}