﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using OxyPlot;
using OxyPlot.Series;

using MathNet.Numerics;
using OxyPlot.Axes;

namespace Decoder
{
    public partial class Plot : Form
    {
        private double[] lnArea { get; set; }
        private double[] forGraph { get; set; }
        private double[] koncentrat { get; set; }
        private double a, b;

        public Plot(double[] _forGraph, double[] _lnArea, double[] _koncentrat)
        {
            InitializeComponent();
            lnArea = _lnArea;
            forGraph = _forGraph;
            koncentrat = _koncentrat;
        }
        
        private void Plot_Load(object sender, EventArgs e)
        {
            double maxLnArea = lnArea.Max();
            for (int i = 0; i < lnArea.Length; i++)
            {
                lnArea[i] = maxLnArea - lnArea[i];
            }

            double[] bufLnArea = new double[0];
            double[] bufKonc = new double[0];
            int p = 0;
            for (int i = 0; i < lnArea.Length; i++)
            {
                if (koncentrat.Length > i)
                {
                    if (koncentrat[i] != 0)
                    {
                        p++;
                        Array.Resize<double>(ref bufLnArea, p);
                        Array.Resize<double>(ref bufKonc, p);
                        bufLnArea[p - 1] = lnArea[i];
                        bufKonc[p - 1] = koncentrat[i];
                    }
                }
            }

            Tuple<double, double> rez = Fit.Line(bufLnArea, bufKonc);
            a = rez.Item1;
            b = rez.Item2;

            LineSeries series1 = new LineSeries();
            series1.MarkerType = MarkerType.Circle;
            series1.MarkerSize = 4d;
            series1.MarkerFill = OxyColor.FromRgb(0, 255, 0);
            series1.LineStyle = LineStyle.None;
            for (int i = lnArea.Length-1; i >-1; i--)
            {
                if (koncentrat.Length > i)
                {
                    if (koncentrat[i] > 0)
                    {
                        series1.Points.Add(new DataPoint(lnArea[i], koncentrat[i]));
                    }
                }
            }

            //Аппроксимация
            LineSeries seriesSpline = new LineSeries();
            for (double i = lnArea.Max(); i > lnArea.Min(); i -= 1)
            {
                seriesSpline.Points.Add(new DataPoint(i, func(i)));
            }

            LineSeries seriesNot = new LineSeries();
            seriesNot.MarkerType = MarkerType.Circle;
            seriesNot.MarkerSize = 4d;
            seriesNot.MarkerFill = OxyColor.FromRgb(255, 0, 0);
            seriesNot.LineStyle = LineStyle.None;
            
            for (int i = lnArea.Length - 1; i > -1; i--)
            {
                if (koncentrat.Length > i)
                {
                    if (koncentrat[i] == 0)
                    {
                        seriesNot.Points.Add(new DataPoint(lnArea[i], func(lnArea[i])));
                    }
                }
            }

            PlotModel model1 = new PlotModel { };
            model1.Series.Add(series1);
            model1.Series.Add(seriesSpline);
            model1.Series.Add(seriesNot);
            model1.Axes.Add(new LinearAxis(AxisPosition.Left, "Концентрация"));
            model1.Axes.Add(new LinearAxis(AxisPosition.Bottom, "Интенсивность"));
            pv1.Model = model1;

            writeResult();
        }


        private double func(double x)
        {
            return a + b * x;
        }


        private void writeResult()
        {
            string col;
            for (int i = 0; i < lnArea.Length; i++)
            {
                if (koncentrat.Length > i)
                {
                    col = koncentrat[i] == 0 ? "Неизвестно" : koncentrat[i].ToString();
                    
                    l_natur.Text += (i + 1).ToString() + ") " + col + "\r\n";

                    l_fut.Text += String.Format("{0}) {1:0.0000} \r\n", (i + 1), func(lnArea[i]));
                    //l_fut.Text += (i + 1).ToString() + ") " + func(lnArea[i]) + "\r\n";
                }
            }
        }

    }
}
