﻿namespace Decoder
{
    partial class SelectWorkArea
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictBox = new System.Windows.Forms.PictureBox();
            this.panel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.l_zoom = new System.Windows.Forms.Label();
            this.trackZoom = new System.Windows.Forms.TrackBar();
            this.btn_cut = new System.Windows.Forms.Button();
            this.l_point = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictBox)).BeginInit();
            this.panel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackZoom)).BeginInit();
            this.SuspendLayout();
            // 
            // pictBox
            // 
            this.pictBox.Location = new System.Drawing.Point(3, 3);
            this.pictBox.Name = "pictBox";
            this.pictBox.Size = new System.Drawing.Size(148, 131);
            this.pictBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictBox.TabIndex = 0;
            this.pictBox.TabStop = false;
            this.pictBox.Paint += new System.Windows.Forms.PaintEventHandler(this.pictBox_Paint);
            this.pictBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel_MouseClick);
            this.pictBox.MouseEnter += new System.EventHandler(this.panel_MouseEnter);
            this.pictBox.MouseLeave += new System.EventHandler(this.pictBox_MouseLeave);
            this.pictBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictBox_MouseMove);
            // 
            // panel
            // 
            this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel.AutoScroll = true;
            this.panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel.Controls.Add(this.label1);
            this.panel.Controls.Add(this.pictBox);
            this.panel.Location = new System.Drawing.Point(0, 33);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(655, 278);
            this.panel.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(9, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "l";
            this.label1.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.l_zoom);
            this.panel1.Controls.Add(this.trackZoom);
            this.panel1.Controls.Add(this.btn_cut);
            this.panel1.Controls.Add(this.l_point);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(655, 30);
            this.panel1.TabIndex = 2;
            // 
            // l_zoom
            // 
            this.l_zoom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.l_zoom.AutoSize = true;
            this.l_zoom.Location = new System.Drawing.Point(345, 13);
            this.l_zoom.Name = "l_zoom";
            this.l_zoom.Size = new System.Drawing.Size(33, 13);
            this.l_zoom.TabIndex = 3;
            this.l_zoom.Text = "100%";
            // 
            // trackZoom
            // 
            this.trackZoom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackZoom.LargeChange = 1;
            this.trackZoom.Location = new System.Drawing.Point(386, 4);
            this.trackZoom.Maximum = 3;
            this.trackZoom.Name = "trackZoom";
            this.trackZoom.Size = new System.Drawing.Size(176, 45);
            this.trackZoom.TabIndex = 2;
            this.trackZoom.Value = 2;
            this.trackZoom.Scroll += new System.EventHandler(this.trackZoom_Scroll);
            // 
            // btn_cut
            // 
            this.btn_cut.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_cut.Enabled = false;
            this.btn_cut.Location = new System.Drawing.Point(568, 4);
            this.btn_cut.Name = "btn_cut";
            this.btn_cut.Size = new System.Drawing.Size(75, 23);
            this.btn_cut.TabIndex = 1;
            this.btn_cut.Text = "Обрезать";
            this.btn_cut.UseVisualStyleBackColor = true;
            this.btn_cut.Click += new System.EventHandler(this.btn_cut_Click);
            // 
            // l_point
            // 
            this.l_point.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.l_point.AutoSize = true;
            this.l_point.Location = new System.Drawing.Point(4, 4);
            this.l_point.Name = "l_point";
            this.l_point.Size = new System.Drawing.Size(336, 13);
            this.l_point.TabIndex = 0;
            this.l_point.Text = "Указать верхний левый и нижний правый угол рабочей области:";
            // 
            // SelectWorkArea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 311);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel);
            this.Name = "SelectWorkArea";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Выбор рабочей области";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SelectWorkArea_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictBox)).EndInit();
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackZoom)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictBox;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label l_point;
        private System.Windows.Forms.Button btn_cut;
        private System.Windows.Forms.TrackBar trackZoom;
        private System.Windows.Forms.Label l_zoom;
    }
}