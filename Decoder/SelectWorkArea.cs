﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Decoder
{
    public partial class SelectWorkArea : Form
    {
        private Image img { get; set; }

        private int[] coordinateX = new int[2],
                      coordinateY = new int[2];
        private int countCoord = 0;

        private Bitmap cutImage;

        public SelectWorkArea(String _path)
        {
            InitializeComponent();            
            img = Image.FromFile(_path);
            panel.MouseWheel += panel_MouseWheel;
        }

        private double kZoom = 1;

        void panel_MouseWheel(object sender, MouseEventArgs e)
        {
            
            
            //label1.Text += "Rotat: " + e.De/ta.ToString();
        }


        private void SelectWorkArea_Load(object sender, EventArgs e)
        {
            pictBox.Image = img;
        }

        private void pictBox_MouseMove(object sender, MouseEventArgs e)
        {
            
            Point pixel = e.Location,
                    pos = Cursor.Position;

            pixel.X =(int)(pixel.X * kZoom);
            pixel.Y =(int)(pixel.Y * kZoom);

            Point textPos = pos;
            textPos.X += 10;
            textPos.Y -= 25;

            label1.Location = textPos;
            label1.Text = "X: " + pixel.X + "; Y: " + pixel.Y;

            
            if (countCoord == 1)
            {
                pictBox.Refresh();
                l_point.Text = "Указать верхний левый и нижний правый угол рабочей области: 1) ( x:" + coordinateX[0] + "; y:" + coordinateX[0] + " ); 2) ( x:" + pixel.X + "; y:" + pixel.Y + " );";
            }
            else if (countCoord == 0)
            {
                l_point.Text = "Указать верхний левый и нижний правый угол рабочей области: 1) ( x:" + pixel.X + "; y:" + pixel.Y + " ); ";
            }

        }

        private void panel_MouseEnter(object sender, EventArgs e)
        {
            label1.Visible = true;
            panel.Focus();
        }
        private void pictBox_MouseLeave(object sender, EventArgs e)
        {
            label1.Visible = false;
        }

        Graphics g = null;
        private void panel_MouseClick(object sender, MouseEventArgs e)
        {

            if(g == null)
            {
                g = pictBox.CreateGraphics();            
            }
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (countCoord < 2)
                {
                    Point coord = e.Location;
                    coord.X = (int)(coord.X * kZoom);
                    coord.Y = (int)(coord.Y * kZoom);
                    coordinateX[countCoord] = coord.X;
                    coordinateY[countCoord] = coord.Y;
                    countCoord++;
                  //  l_point.Text += " Точка " + countCoord.ToString() + ") x: " + coord.X + "; y: " + coord.Y + "; ";

                    pictBox.Refresh();
                    if (countCoord == 2)
                    {
                        btn_cut.Enabled = true;
                    }
                }
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                coordinateX = new int[2];
                coordinateY = new int[2];
                countCoord = 0;
                btn_cut.Enabled = false;
                l_point.Text = "Указать верхний левый и нижний правый угол рабочей области: ";
                pictBox.Invalidate();
            }
        }

        private void btn_cut_Click(object sender, EventArgs e)
        {
            Point pointStart = new Point(coordinateX[0], coordinateY[0]),
                  pointEnd = new Point(coordinateX[1], coordinateY[1]);


            int _width = pointEnd.X - pointStart.X,
                _heigt = pointEnd.Y - pointStart.Y;


            if (_width < _heigt)
            {
                cutImage = readVertical();
                
            }
            else
            {
                cutImage = readHorizontal();          
            }
            ((Form1)Application.OpenForms["Form1"]).Img = cutImage;
            ((Button)((Form1)Application.OpenForms["Form1"]).Controls["btn_findPoint"]).Enabled = true;
            this.Close();
        }

        /// <summary>
        /// Вертикальное выделение
        /// </summary>
        /// <returns></returns>
        private Bitmap readVertical()
        {
            Point pointStart = new Point(coordinateX[0], coordinateY[0]),
                  pointEnd = new Point(coordinateX[1], coordinateY[1]);

            Bitmap newImg,
                   oldImg = (Bitmap)img;

            int _width = pointEnd.X - pointStart.X,
                _heigt = pointEnd.Y - pointStart.Y,
                startX = pointStart.X,
                startY = pointStart.Y;

            newImg = new Bitmap(_width, _heigt);
            Color pix;
            for (int i = 0; i < _heigt; i++)
            {
                int x = startX;
                for (int j = 0; j < _width; j++)
                {
                    pix = oldImg.GetPixel(x, startY);
                    newImg.SetPixel(j, i, pix);
                    x++;
                }
                startY++;
            }
            return newImg;
        }

        /// <summary>
        /// Горизонтальное выделение
        /// </summary>
        /// <returns></returns>
        private Bitmap readHorizontal()
        {
            Point pointStart = new Point(coordinateX[0], coordinateY[0]),
                  pointEnd = new Point(coordinateX[1], coordinateY[1]);

            Bitmap newImg,
                   oldImg = (Bitmap)img;

            int _width = pointEnd.X - pointStart.X,
                _heigt = pointEnd.Y - pointStart.Y,
                startX = pointStart.X,
                startY = pointEnd.Y;

            newImg = new Bitmap(_heigt, _width);
            Color pix;
            for (int i = 0; i < _width; i++)
            {
                int y = pointEnd.Y;
                for (int j = 0; j < _heigt; j++)
                {
                    pix = oldImg.GetPixel(startX, y);
                    newImg.SetPixel(j, i, pix);
                    y--;
                }
                startX++;
            }
            return newImg;
        }

        private void trackZoom_Scroll(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            int val = ((TrackBar)sender).Value;

            double zoomFactor = getZoom(val);
          //  Bitmap originalBitmap = (Bitmap)pictBox.Image;
            l_zoom.Text = zoomFactor * 100 + "%";
            Size newSize = new Size((int)(img.Width * zoomFactor), (int)(img.Height * zoomFactor));
            Bitmap bmp = new Bitmap(img, newSize);

            pictBox.Image = bmp;
            Cursor = Cursors.Default;
        }

        private double getZoom(int val)
        {
            double _val = 1;
            switch (val)
            {
                case 0:
                    _val = 0.25;
                    break;
                case 1:
                    _val = 0.5;
                    break;
                case 2:
                    _val = 1;
                    break;
                case 3:
                    _val = 1.5;
                    break;
            }
            kZoom = 1 / _val;
            return _val;
        }

        System.Drawing.SolidBrush myBrush = new SolidBrush(Color.FromArgb(200, Color.Black));
        private void pictBox_Paint(object sender, PaintEventArgs e)
        {

            Pen p = new Pen(myBrush);
            p.DashStyle = System.Drawing.Drawing2D.DashStyle.DashDot;
            if (countCoord == 2)
            {
                e.Graphics.DrawRectangle(p, (float)(coordinateX[0] / kZoom), (float)(coordinateY[0] / kZoom), (float)((coordinateX[1] / kZoom) - (coordinateX[0] / kZoom)), (float)((coordinateY[1] / kZoom) - (coordinateY[0] / kZoom)));
            }
            else if (countCoord == 1)
            {
                Point loc = pictBox.PointToClient( Cursor.Position);
                e.Graphics.DrawRectangle(p, (float)(coordinateX[0] / kZoom), (float)(coordinateY[0] / kZoom), (float)((loc.X) - (coordinateX[0] / kZoom)), (float)((loc.Y) - (coordinateY[0] / kZoom)));
            }
        }



    }
}
