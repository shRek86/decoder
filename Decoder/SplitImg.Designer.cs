﻿namespace Decoder
{
    partial class SplitImg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picBox = new System.Windows.Forms.PictureBox();
            this.plot = new OxyPlot.WindowsForms.PlotView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_success = new System.Windows.Forms.Button();
            this.l_resuzt = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tb_delta_M2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_count_point = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_shum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_start = new System.Windows.Forms.Button();
            this.cb_metod = new System.Windows.Forms.ComboBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label_position = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // picBox
            // 
            this.picBox.Enabled = false;
            this.picBox.Location = new System.Drawing.Point(87, 12);
            this.picBox.Name = "picBox";
            this.picBox.Size = new System.Drawing.Size(845, 172);
            this.picBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox.TabIndex = 0;
            this.picBox.TabStop = false;
            this.picBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.picBox_MouseClick);
            this.picBox.MouseLeave += new System.EventHandler(this.picBox_MouseLeave);
            this.picBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picBox_MouseMove);
            // 
            // plot
            // 
            this.plot.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.plot.Enabled = false;
            this.plot.Location = new System.Drawing.Point(12, 190);
            this.plot.Name = "plot";
            this.plot.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.plot.Size = new System.Drawing.Size(930, 221);
            this.plot.TabIndex = 1;
            this.plot.Text = "plotView1";
            this.plot.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.plot.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.plot.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Location = new System.Drawing.Point(7, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(282, 233);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Результат";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.btn_success);
            this.panel1.Controls.Add(this.l_resuzt);
            this.panel1.Location = new System.Drawing.Point(6, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(270, 208);
            this.panel1.TabIndex = 2;
            // 
            // btn_success
            // 
            this.btn_success.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_success.Enabled = false;
            this.btn_success.Location = new System.Drawing.Point(190, 182);
            this.btn_success.Name = "btn_success";
            this.btn_success.Size = new System.Drawing.Size(75, 23);
            this.btn_success.TabIndex = 4;
            this.btn_success.Text = "Применить";
            this.btn_success.UseVisualStyleBackColor = true;
            this.btn_success.Click += new System.EventHandler(this.btn_success_Click);
            // 
            // l_resuzt
            // 
            this.l_resuzt.AutoSize = true;
            this.l_resuzt.Location = new System.Drawing.Point(3, 0);
            this.l_resuzt.Name = "l_resuzt";
            this.l_resuzt.Size = new System.Drawing.Size(0, 13);
            this.l_resuzt.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.tb_delta_M2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.tb_count_point);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.tb_shum);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.btn_start);
            this.groupBox2.Controls.Add(this.cb_metod);
            this.groupBox2.Location = new System.Drawing.Point(7, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(282, 149);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Метод распознавания";
            // 
            // tb_delta_M2
            // 
            this.tb_delta_M2.Enabled = false;
            this.tb_delta_M2.Location = new System.Drawing.Point(160, 77);
            this.tb_delta_M2.Name = "tb_delta_M2";
            this.tb_delta_M2.Size = new System.Drawing.Size(78, 20);
            this.tb_delta_M2.TabIndex = 5;
            this.tb_delta_M2.Text = "3,66";
            this.tb_delta_M2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(157, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Дельта:";
            this.label3.Visible = false;
            // 
            // tb_count_point
            // 
            this.tb_count_point.Location = new System.Drawing.Point(198, 31);
            this.tb_count_point.Name = "tb_count_point";
            this.tb_count_point.Size = new System.Drawing.Size(78, 20);
            this.tb_count_point.TabIndex = 1;
            this.tb_count_point.TextChanged += new System.EventHandler(this.tb_count_point_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(195, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Кол-во пятен:";
            // 
            // tb_shum
            // 
            this.tb_shum.Enabled = false;
            this.tb_shum.Location = new System.Drawing.Point(12, 77);
            this.tb_shum.Name = "tb_shum";
            this.tb_shum.Size = new System.Drawing.Size(78, 20);
            this.tb_shum.TabIndex = 6;
            this.tb_shum.Text = "1000";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Уровень шума:";
            // 
            // btn_start
            // 
            this.btn_start.Enabled = false;
            this.btn_start.Location = new System.Drawing.Point(170, 119);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(106, 23);
            this.btn_start.TabIndex = 3;
            this.btn_start.Text = "Распознать";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // cb_metod
            // 
            this.cb_metod.FormattingEnabled = true;
            this.cb_metod.Items.AddRange(new object[] {
            "Автоматический (Метод 1)",
            "Автоматический (Метод 2)",
            "Ручной"});
            this.cb_metod.Location = new System.Drawing.Point(6, 31);
            this.cb_metod.Name = "cb_metod";
            this.cb_metod.Size = new System.Drawing.Size(160, 21);
            this.cb_metod.TabIndex = 0;
            this.cb_metod.SelectedIndexChanged += new System.EventHandler(this.cb_metod_SelectedIndexChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(948, 12);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Size = new System.Drawing.Size(292, 399);
            this.splitContainer1.SplitterDistance = 156;
            this.splitContainer1.TabIndex = 2;
            // 
            // label_position
            // 
            this.label_position.AutoSize = true;
            this.label_position.Location = new System.Drawing.Point(68, 16);
            this.label_position.Name = "label_position";
            this.label_position.Size = new System.Drawing.Size(0, 13);
            this.label_position.TabIndex = 3;
            this.label_position.Visible = false;
            // 
            // SplitImg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1252, 423);
            this.Controls.Add(this.label_position);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.plot);
            this.Controls.Add(this.picBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "SplitImg";
            this.ShowIcon = false;
            this.Text = "Распознавание пятен";
            this.Load += new System.EventHandler(this.SplitImg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picBox;
        private OxyPlot.WindowsForms.PlotView plot;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_success;
        private System.Windows.Forms.Label l_resuzt;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tb_delta_M2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_count_point;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_shum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.ComboBox cb_metod;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label_position;
    }
}