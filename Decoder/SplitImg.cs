﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Annotations;
using OxyPlot.Axes;

namespace Decoder
{
    public partial class SplitImg : Form
    {

        private Bitmap img, rotateImg;
        double[] lnDrawGraf, sumImg, lnSumImg;
        int[] borderPoint;
        const double aR = 1.83,
                     aG = 2.34,
                     aB = 2.69;
        private PlotModel model;

        double kZoom = 1;
        int width, height;

        Point[] borderPoints;

        public SplitImg(Bitmap _img)
        {
            InitializeComponent();
            img = _img;
            rotateImg = new Bitmap(_img);
        }

        private void SplitImg_Load(object sender, EventArgs e)
        {
            LoadImg();
        }

        /// <summary>
        /// Отображение фото и графика
        /// </summary>
        private void LoadImg()
        {
            Cursor = Cursors.WaitCursor;
            rotateImg.RotateFlip(RotateFlipType.Rotate270FlipNone);
            picBox.Image = rotateImg;

            width = img.Width;
            height = img.Height;

            kZoom = double.Parse(height.ToString()) / picBox.Width;

            lnDrawGraf = new double[height];
            sumImg = new double[height];
            lnSumImg = new double[height];

            LineSeries lineSe = new LineSeries();
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    Color col = img.GetPixel(j, i);
                    sumImg[i] += col.R * aR + col.G * aG + col.B * aB;
                    lnSumImg[i] += Math.Log(col.R * aR + col.G * aG + col.B * aB);
                    //lnDrawGraf[i] += Math.Log((255 - col.R) * aR + (255 - col.G) * aG + (255 - col.B) * aB);
                    
                    //lnDrawGraf[i] += Math.Log(col.R * aR + col.G * aG + col.B * aB);
                   // lnDrawGraf[i] += (255 - col.R) * aR + (255 - col.G) * aG + (255 - col.B) * aB;
                }
            }

            double MaxBackg = lnSumImg.Max();

            for (int i = 0; i < height; i++)
            {
                lnDrawGraf[i] = MaxBackg - lnSumImg[i];
                lineSe.Points.Add(new DataPoint(i, lnDrawGraf[i]));
            }

            model = new PlotModel();
            model.Series.Add(lineSe);
            model.Axes.Add(new LinearAxis(AxisPosition.Left, "Интенсивность"));
            model.Axes.Add(new LinearAxis(AxisPosition.Bottom, "Пиксель"));
            plot.Model = model;
            Cursor = Cursors.Default;

        }

        /// <summary>
        /// Мой метод (поиск пиков)
        /// </summary>
        private int[] Metod1(double[] _valArray, double delta, int countPoint)
        {
            double[] buf = new double[0];
            double backgr = _valArray.Max();
            int k = 0;
            long index = 0;
            double[] bufVal;
            try
            {
                for (int i = 0; i < countPoint; i++)
                {
                    bufVal = new double[_valArray.Length - index];
                    Array.Copy(_valArray, index, bufVal, 0, _valArray.Length - index);
                    double min = bufVal.Min();
                    k++;
                    Array.Resize<double>(ref buf, k);
                    buf[k - 1] = min;
                    index = Array.IndexOf(_valArray, min);
                    while (index < _valArray.Length && _valArray[index] < (backgr - delta))
                    {
                        index++;
                    }
                }
            }
            catch (Exception e)
            {
            }
            int[] indexs = new int[k];   //Пиксель пика
            for (int i = 0; i < k; i++)
            {
                indexs[i] = Array.IndexOf(_valArray, buf[i]);
            }

            int[] borderPoint = new int[k-1]; //Середина между точками
            for (int i = 0; i < k - 1; i++)
            {
                borderPoint[i] = (indexs[i+1] + indexs[i])/2;
            }
            
            ///Вывод информации(результата)
            l_resuzt.Text = "Найдено пятен: " + k + "\r\n===================\r\nЦентр пятна в точке:";
            for (int i = 0; i < k; i++)
            {
                l_resuzt.Text += "\r\n" + (i + 1) + ": " + indexs[i];
            }
            l_resuzt.Text += "\r\n===================\r\nГраница пятен:";
            for (int i = 0; i < k-1; i++)
            {
                l_resuzt.Text += "\r\n" + (i + 1) + ": " + borderPoint[i];
            } 

            return borderPoint;
        }

        /// <summary>
        /// Метод М.Г. Выделение границ пятен
        /// </summary>
        private int[] Metod2(double[] _array, double eps, int pointCount)
        {
            double max = _array.Max(),
                  min = _array.Min(),
                  h = (max + min) / 2;
            int count = 0, arrayCount = 0;
            int[] points = new int[0];
            bool down = true;

            while ((max - min) > eps)
            {
                count = 0;
                arrayCount = 0;
                Array.Resize<int>(ref points, 0);
                for (int i = 0; i < _array.Length; i++)
                {
                    if (down)
                    {
                        if ((_array[i] - h) < 0)
                        {
                            down = !down;
                            arrayCount++;
                            Array.Resize<int>(ref points, arrayCount);
                            points[arrayCount - 1] = i;
                        }
                    }
                    else
                    {
                        if ((_array[i] - h) > 0)
                        {
                            down = !down;
                            arrayCount++;
                            Array.Resize<int>(ref points, arrayCount);
                            points[arrayCount - 1] = i;
                            count++;
                        }
                    }
                }

                if (count > pointCount)
                {
                    max = h;
                }
                else
                {
                    min = h;
                }
                h = (max + min) / 2;
            }

            int[] borderPoint = new int[count - 1];
            int j = 0;
            for (int i = 1; i < arrayCount-1; i += 2)
            {
                borderPoint[j] = (points[i] + points[i+1])/2;
                j++;
            }

            ///Вывод информации(результата)
            l_resuzt.Text = "Найдено пятен: " + count + "\r\n===================\r\nЛокальные границы пятен:";
            borderPoints = new Point[count];
            j = 1;
            for (int i = 0; i < arrayCount; i +=2)
            {
                borderPoints[j - 1].X = points[i];
                borderPoints[j - 1].Y = points[i+1];
                l_resuzt.Text += "\r\n" + j + ": (" + points[i] + "; " + points[i+1] + ")";
                j++;
            }
            l_resuzt.Text += "\r\n===================\r\nГраница пятен:";
            for (int i = 0; i < count - 1; i++)
            {
                l_resuzt.Text += "\r\n" + (i + 1) + ": " + borderPoint[i];
            }

            /*LineSeries hLine = new LineSeries();
            hLine.Points.Add(new DataPoint(0, h));
            hLine.Points.Add(new DataPoint(_array.Length, h));
            model.Series.Add(hLine);*/

            return borderPoint;
        }

        private void cb_metod_SelectedIndexChanged(object sender, EventArgs e)
        {
            tb_shum.Enabled = false;
            tb_delta_M2.Enabled = false;
            
            picBox.Enabled = false;
            picBox.BorderStyle = BorderStyle.None;

            btn_start.Enabled = true;
            tb_count_point.Enabled = true;

            l_resuzt.Text = "";
            model.Annotations.Clear();
            plot.InvalidatePlot(true);
            btn_success.Enabled = false;

            if (cb_metod.SelectedIndex == 0)
            {
                tb_shum.Enabled = true;
            }
            else if (cb_metod.SelectedIndex == 1)
            {
                tb_delta_M2.Enabled = true;
            }
            else
            {
                picBox.Enabled = true;
                picBox.BorderStyle = BorderStyle.Fixed3D;
                btn_start.Enabled = false;
                tb_count_point.Enabled = false;
                Array.Resize<int>(ref borderPoint, 0);
            }

        }

        private void btn_success_Click(object sender, EventArgs e)
        {
            ((Form1)Application.OpenForms["Form1"]).LnGraph = lnSumImg;
            //((Form1)Application.OpenForms["Form1"]).LnGraph = lnDrawGraf;
            ((Form1)Application.OpenForms["Form1"]).InverseGraph = lnDrawGraf;
            ((Form1)Application.OpenForms["Form1"]).BorderIndex = borderPoint;

            ((Form1)Application.OpenForms["Form1"]).BorderPoint = borderPoints;

            ((Form1)Application.OpenForms["Form1"]).startCreateTB(int.Parse(tb_count_point.Text));
            ((Form1)Application.OpenForms["Form1"]).Focus();
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            int countPoint = int.Parse(tb_count_point.Text);
            
            borderPoint = new int[0];
            switch (cb_metod.SelectedIndex)
            {
                case 0:
                    double shum = double.Parse(tb_shum.Text);
                    borderPoint = Metod1(sumImg, shum, countPoint);
                    break;
                case 1:
                    double eps = double.Parse(tb_delta_M2.Text.Replace('.',','));
                    borderPoint = Metod2(sumImg, eps, countPoint);
                    break;
                case 2:
                    break;
            }

            if (borderPoint.Length > 0)
            {
                addAnotationOnPlot(borderPoint);
                btn_success.Enabled = true;
            }
            else
            {
                btn_success.Enabled = false;
            }
        }

        /// <summary>
        /// Отображает границы точек на графике
        /// </summary>
        /// <param name="borderPoint">Массив с разделяющими значениями</param>
        private void addAnotationOnPlot(int[] borderPoint)
        {
            Random rnd = new Random();

            int minY = (int)lnDrawGraf.Min(),
                maxY = (int)lnDrawGraf.Max(),
                minX = 0, maxX;
            model.Annotations.Clear();
            for (int i = 0; i < borderPoint.Length + 1; i++)
            {
                PolygonAnnotation pol = new PolygonAnnotation();
                pol.Layer = AnnotationLayer.BelowAxes;
                pol.StrokeThickness = 2;
                pol.Stroke = OxyColor.FromRgb((byte)rnd.Next(256), (byte)rnd.Next(100), (byte)rnd.Next(256));
                pol.Fill = OxyColor.FromAColor(100, pol.Stroke);
                pol.LineStyle = LineStyle.Solid;

                if (i == borderPoint.Length)
                {
                    maxX = height;
                }
                else
                {
                    maxX = borderPoint[i];
                }

                pol.Points.Add(new DataPoint(minX, minY));
                pol.Points.Add(new DataPoint(maxX, minY));
                pol.Points.Add(new DataPoint(maxX, maxY));
                pol.Points.Add(new DataPoint(minX, maxY));

                minX = maxX;
                model.Annotations.Add(pol);
            }
            plot.InvalidatePlot(true);
        }

        private void tb_count_point_TextChanged(object sender, EventArgs e)
        {
            if (tb_count_point.Enabled)
            {
                btn_success.Enabled = false;
            }
        }


         /************************************************\
         /** Функции для ручного разбиения изображения  **\
         /************************************************\*/
        private void picBox_MouseMove(object sender, MouseEventArgs e)
        {
            Point pixel = e.Location,
                // pos = Cursor.Position;
                   pos = e.Location;
            
            pixel.X = (int)(pixel.X * kZoom);
            pixel.Y = (int)(pixel.Y * kZoom);

            Point textPos = pos;
            textPos.X += picBox.Location.X + 10;
            textPos.Y -= picBox.Location.Y - 50;

            label_position.Location = textPos;
            label_position.Text = pixel.X.ToString();
            label_position.Visible = true;
        }

        private void picBox_MouseLeave(object sender, EventArgs e)
        {
            label_position.Visible = false;
            label_position.Text = "";
        }

        private void picBox_MouseClick(object sender, MouseEventArgs e)
        {
            if (borderPoint == null)
            {
                borderPoint = new int[0];
            }
            int countElem = borderPoint.Length;
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                countElem++;
                Array.Resize<int>(ref borderPoint, countElem);
                borderPoint[countElem - 1] = int.Parse(label_position.Text);
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (countElem > 0)
                {
                    countElem--;
                    Array.Resize<int>(ref borderPoint, countElem);
                }
            }

            l_resuzt.Text = "";
            if (countElem == 0)
            {
                model.Annotations.Clear();
                plot.InvalidatePlot(true);
                btn_success.Enabled = false;
                tb_count_point.Text = "0";
            }
            else
            {
                addAnotationOnPlot(borderPoint);
                btn_success.Enabled = true;
                tb_count_point.Text = (countElem+1).ToString();
                l_resuzt.Text += "\r\n===================\r\nГраница пятен:";
                for (int i = 0; i < countElem; i++)
                {
                    l_resuzt.Text += "\r\n" + (i + 1) + ": " + borderPoint[i];
                } 
            }
            
        }

    }
}
